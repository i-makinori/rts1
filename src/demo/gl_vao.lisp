
(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload '("cl-opengl" "cl-glu" "cl-glut")))

(defpackage #:gl-vao
  (:use #:common-lisp)
  (:export #:cube))

(in-package #:gl-vao)
