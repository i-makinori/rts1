
(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload '("iup" "iup-gl" "cl-opengl" "cl-glu")))

(defpackage #:iup-demo
  (:use #:common-lisp)
  (:export #:run-cube))

(in-package #:iup-demo)


;;;;

(defvar *canvas* nil)
(defvar *label1* nil)
(defvar *tt* 0.0)

(defvar *vertices*
  #((-1 -1 1) (-1 1 1)
    (1 1 1) (1 -1 1)
    (-1 -1 -1) (-1 1 -1)
    (1 1 -1) (1 -1 -1)))

(defun polygon (a b c d)
  (gl:begin :polygon)
  (apply #'gl:vertex (aref *vertices* a))
  (apply #'gl:vertex (aref *vertices* b))
  (apply #'gl:vertex (aref *vertices* c))
  (apply #'gl:vertex (aref *vertices* d))
  (gl:end))

(defun color-cube ()
  (gl:color 1 0 0)  (gl:normal 1 0 0)  (polygon 2 3 7 6)
  (gl:color 0 1 0)  (gl:normal 0 1 0)  (polygon 1 2 6 5)
  (gl:color 0 0 1)  (gl:normal 0 0 1)  (polygon 0 3 2 1)
  (gl:color 1 0 1)  (gl:normal 0 -1 0)  (polygon 3 0 4 7)
  (gl:color 1 1 0)  (gl:normal 0 0 -1)  (polygon 4 5 6 7)
  (gl:color 0 1 1)  (gl:normal -1 0 0)  (polygon 5 4 0 1))





(defparameter *px* 8)
(defparameter *py* 8)
(defparameter *pz* 8)


(defun reset-label-text (ilabel text)
  (setf (iup:attribute ilabel :title)  text)
  
  )

(defparameter *counter* 0)

(defun cube ()
  (iup:with-iup ()
    (iup-gl:open)
    (setf *canvas*
          (iup-gl:canvas :rastersize "640x480"
                         :buffer "DOUBLE"
                         :action 'repaint
                         :resize_cb 'resize
                         :keypress_cb 'keypress
                         :motion_cb (lambda (ih x y status)
                                      (reset-label-text
                                       *label1*
                                       (format nil "~A ~A ~A ~A~%" ih x y status))
                                      iup:+default+)))
    
    (setf *label1* (iup:label :title ""))
    (let*
        ((button-left
           (iup:button :title "<-"
                       :action (lambda (handle) handle (incf *px* +1) iup:+default+)))
         (button-right
           (iup:button :title "->"
                       :action (lambda (handle) handle (incf *px* -1) iup:+default+)))

         (widgets
           (iup:vbox (list *canvas*
                           (iup:hbox (list button-left button-right *label1*)
                                     :alignment :acenter))
                     :alignment :acenter))
         (dialog (iup:dialog widgets :title "GL sample")))
      ;; canvas gl
      (iup-cffi::%iup-set-function :idle_action (cffi:callback idle-cb))
      (setf (iup:attribute *canvas* :depthsize) "16")
      
      ;; widgets
      #|
      (setf (iup:idle-action)
            (lambda ()
              (setf *counter* (1+ *counter*))
              (reset-label-text *label1*
                                (format nil "~A~%" *counter*))
              ;;(sleep 0.1)
      iup:+default+))
      |#
      
      ;; start loop
      (iup:show dialog)
      (iup:main-loop))))


(defun keypress (ihandle c press)
  ihandle c press
  ;;(format t "~A, ~A, ~A~%" ih c press)
  iup::+default+
)

(cffi:defcallback idle-cb :int ()
  (incf *tt* 1.0)
  (iup-gl:make-current *canvas*)
  (repaint *canvas* 0 0)
  
  ;;(iup-gl:wait 100)
  (sleep 0.02)
  iup::+default+)


(defparameter *time-ago* 0)


(defun repaint (handle posx posy)
  ;; frame counter
  (let ((time-almost-now (get-internal-real-time)))
    (setf *counter* (1+ *counter*))
    (reset-label-text *label1*
                      (format nil "FPS: ~4,f, ~A"
                              (handler-case
                                  (float (/ 1 (/ (- time-almost-now *time-ago*) internal-time-units-per-second)))
                                (division-by-zero (e) e 0))
                              *counter*))
    (setf *time-ago* time-almost-now) ;; for under second
    )

  ;; graphics update
  (iup-gl:make-current handle)
  
  (gl:clear-color 0.3 0.3 0.3 1.0)
  (gl:clear :color-buffer-bit :depth-buffer-bit)
  
  (gl:matrix-mode :modelview)
  (gl:with-pushed-matrix
    (gl:translate 0 0 0)
    (gl:scale 1 1 1)
    (gl:rotate *tt* 0 0 1)
    (color-cube))

  (gl:load-identity)
  (glu:look-at *px* *py* *pz*
               0 0 0
               0 1 0)

  (gl:matrix-mode :modelview)


  (iup-gl:swap-buffers handle)
  iup::+default+
  )

(defun resize (handle width height)
  (iup-gl:make-current handle)
  (gl:viewport 0 0 width height)
  (gl:enable :depth-test)
  ;;
  (gl:matrix-mode :modelview)
  (gl:load-identity)
  ;;
  (gl:matrix-mode :projection)
  (gl:load-identity)
  (glu:perspective 60 (/ width height) 1 150)    
  iup::+default+)


(defun run-cube ()
  #-sbcl
  (cube)
  #+sbcl
  (sb-int:with-float-traps-masked
      (:divide-by-zero :invalid)
    (cube))
  )
