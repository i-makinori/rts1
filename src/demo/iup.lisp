
(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload '("iup" "iup-gl" "cl-opengl" "cl-glu" "cl-glut")))

(defpackage #:iup-gl-tools
  (:use #:common-lisp)
  (:export #:run-cube))

(in-package #:iup-gl-tools)

;;;; iup


(defvar *canvas* nil)
(defvar *tt* 0.0)

(defvar *vertices*
  #((-1 -1 1) (-1 1 1)
    (1 1 1) (1 -1 1)
    (-1 -1 -1) (-1 1 -1)
    (1 1 -1) (1 -1 -1)))

(defun polygon (a b c d)
  (gl:begin :polygon)
  (apply #'gl:vertex (aref *vertices* a))
  (apply #'gl:vertex (aref *vertices* b))
  (apply #'gl:vertex (aref *vertices* c))
  (apply #'gl:vertex (aref *vertices* d))
  (gl:end))

(defun color-cube ()
  (gl:color 1 0 0)  (gl:normal 1 0 0)  (polygon 2 3 7 6)
  (gl:color 0 1 0)  (gl:normal 0 1 0)  (polygon 1 2 6 5)
  (gl:color 0 0 1)  (gl:normal 0 0 1)  (polygon 0 3 2 1)
  (gl:color 1 0 1)  (gl:normal 0 -1 0)  (polygon 3 0 4 7)
  (gl:color 1 1 0)  (gl:normal 0 0 -1)  (polygon 4 5 6 7)
  (gl:color 0 1 1)  (gl:normal -1 0 0)  (polygon 5 4 0 1))



(defparameter *rot* 0)

(defparameter *d-text* "")

(defun cube ()
  (iup:with-iup ()
    (iup-gl:open)
    (setf *canvas*
          (iup-gl:canvas :rastersize "640x480"
                         :buffer "DOUBLE"
                         :action 'repaint
                         :resize_cb 'resize
                         :keypress_cb 'keypress))
    (let*
        ((button-left
           (iup:button :title "<-"
                       :action (lambda (handle)
                                 handle
                                 (incf *rot* +1)
                                 iup:+default+)))
         (button-right
           (iup:button :title "->"
                       :action (lambda (handle)
                                 handle
                                 (incf *rot* -1)
                                 iup:+default+)))
         (debug-text
           (iup:label :title *d-text*))
         (widgets
           (iup:vbox (list *canvas*
                           (iup:hbox (list button-left button-right debug-text)
                                     :alignment :acenter))
                     :alignment :acenter))
         (dialog (iup:dialog widgets :title "GL sample")))
      ;; canvas gl
      (iup-cffi::%iup-set-function :idle_action (cffi:callback idle-cb))
      (setf (iup:attribute *canvas* :depthsize) "16")
      
      ;; widgets
      
      ;; start loop
      (iup:show dialog)
      (iup:main-loop))))


(defun keypress (ih c press)
  ih press c
  ;;(format t "~A, ~A, ~A~%" ih c press)
  iup::+default+
)

(cffi:defcallback idle-cb :int ()
  (incf *tt* 1.0)
  (iup-gl:make-current *canvas*)
  (repaint *canvas* 0 0)
  
  ;;(iup-gl:wait 100)
  (sleep 0.02)
  iup::+default+)

;;(defun repaint (handle posx posy)
(defun repaint (handle posx posy)  
  (setf *d-text* (format nil "~A, ~A, ~A%" handle posx posy))
  
  (iup-gl:make-current handle)
  
  (gl:clear-color 0.3 0.3 0.3 1.0)
  (gl:clear :color-buffer-bit :depth-buffer-bit)
  (gl:enable :depth-test)
  
  (gl:matrix-mode :modelview)
  (gl:with-pushed-matrix
    (gl:translate 0 0 0)
    (gl:scale 1 1 1)
    (gl:rotate *tt* 0 0 1)
    (color-cube))

  (gl:load-identity)
  (glu:look-at 8 8 8
               0 0 0
               0 1 0)

  (gl:matrix-mode :modelview)


  (iup-gl:swap-buffers handle)
  iup::+default+
  )

(defun resize (handle width height)
  (iup-gl:make-current handle)
  (gl:viewport 0 0 width height)
  (gl:matrix-mode :modelview)
  (gl:load-identity)
  (gl:matrix-mode :projection)
  (gl:load-identity)
  (glu:perspective 60 (/ width height) 1 150)    
  iup::+default+)


(defun run-cube ()
  #-sbcl
  (cube)
  #+sbcl
  (sb-int:with-float-traps-masked
      (:divide-by-zero :invalid)
    (cube))
  )
