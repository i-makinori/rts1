(defpackage :rts1
  (:use
   :cl
   ;; :sdl2
   ;; opengl
   :cl-opengl
   ;; :cl-glu
   ;; :gtk ;; cl-cffi-gtk
   ;;:gtk 
   ))

(in-package :rts1)

;;;; utils

(defun debug-log (msg &rest args)
  "Output and flush MSG to STDOUT with arguments ARGS"
  (apply #'format t msg args)
  ;; Flush to standard out
  (finish-output))

;;;; main

(defparameter *width* 1200)
(defparameter *height* 800)

(defun main ()
  "The entry point of our game."
  (sdl2:with-init (:everything)
    (debug-log "Using SDL library version: ~D.~D.~D~%"
               sdl2-ffi:+sdl-major-version+
               sdl2-ffi:+sdl-minor-version+
               sdl2-ffi:+sdl-patchlevel+)
 
    (sdl2:with-window (window :flags '(:shown :opengl) :w *width* :h *height*)
      (sdl2:with-gl-context (gl-context window)
        ;; Basic window/gl setup
        (setup-gl window gl-context)
 
        ;; Run main loop
        (main-loop window #'render)))))

(defun draw-axis ()
  (gl:with-pushed-matrix
    (gl:begin :lines)
    (gl:color 1 0 0) (gl:vertex 0 0 0) (gl:vertex 10 0 0)
    (gl:color 0 1 0) (gl:vertex 0 0 0) (gl:vertex 0 10 0)
    (gl:color 0 0 1) (gl:vertex 0 0 0) (gl:vertex 0 0 10)
    (gl:end)
    )
  )
 
(defun setup-gl (window gl-context)
  "Setup OpenGL with the window WIN and the gl context of GL-CONTEXT"
  
  ;; sdl2
  (debug-log "Setting up window/gl.~%")
  (sdl2:gl-make-current window gl-context)
  
  ;; opengl
  (gl:clear-color 0 0.1 0.1 0.1)
  (gl:color 0 0 0)

  (gl:matrix-mode :projection)
  (gl:load-identity)
  (glu:perspective 60 (/ 4 3) 1 40) ;; camera

  )

(defparameter *counter* 0)

(defun render ()
  
  ;; clear
  (gl:clear :color-buffer-bit)
  (gl:clear-color 0 0.1 0.1 0.1)
  
  ;; render shapes
  (gl:matrix-mode :modelview)
  (draw-axis)

  (gl:begin :lines)
  (gl:color 0.5 0.5 0.5)
  (gl:vertex 0 0 0)
  (gl:vertex 5 5 (sin (* *counter* 0.04)))
  (gl:end)

  ;; look
  (gl:matrix-mode :modelview)
  (gl:load-identity)
  (glu:look-at (* 8 (cos (* 0.01 *counter*))) 8 (* 8 (sin (* 0.01 *counter*)))
               0 0 0
               0 10 0)

  ;; flush
  (gl:flush)
  )

 
(defun main-loop (window render-fn)
  "Run the game loop that handles input, rendering through the
  render function RENDER-FN, amongst others."
  (sdl2:with-event-loop (:method :poll)
    (:idle ()
      (funcall render-fn)
      ;; Swap back buffer
      (sdl2:gl-swap-window window)
      (incf *counter*)
      )
    (:keydown
     (:keysym keysym)
     (case (sdl2:scancode keysym)
       (:scancode-s (incf *counter* -5))
       (:scancode-a (incf *counter* 5))))
    (:quit () t)))
