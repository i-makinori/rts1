
(in-package :rts1)


;;;;

;;;; utils

(defun replace-all (string part replacement &key (test #'char=))
"Returns a new string in which all the occurences of the part 
is replaced with replacement."
    (with-output-to-string (out)
      (loop with part-length = (length part)
            for old-pos = 0 then (+ pos part-length)
            for pos = (search part string
                              :start2 old-pos
                              :test test)
            do (write-string string out
                             :start old-pos
                             :end (or pos (length string)))
            when pos do (write-string replacement out)
              while pos)))



;;;; glfw
(defun quit-simulator ()
  ;; buffer objects
  
  ;; glfw
  (glfw:set-window-should-close)
  
  ;; threads, process
  )


(defparameter *pressing-keys* '())
(defparameter *just-pressed-keys* '())

(glfw:def-key-callback updatekeyboard-status (window key scancode action mod-keys)
  (declare (ignore window scancode mod-keys))
  (when (and (eq key :escape) (eq action :press))
    (quit-simulator)
    )
  (when (eq action :press)
    (setq *just-pressed-keys* (cons key *just-pressed-keys*))
    (setq *pressing-keys* (cons key *pressing-keys*)))
  (when (eq action :release)
    (setq *pressing-keys* (remove key *pressing-keys*)))
  )





(glfw:def-key-callback quit-on-escape (window key scancode action mod-keys)
  (declare (ignore window scancode mod-keys))
  (when (and (eq key :escape) (eq action :press))
    (quit-simulator)
    ))


(defun render-axis ()
  (gl:with-pushed-matrix
    (gl:begin :lines)
    (gl:color 1 0 0) (gl:vertex 0 0 0) (gl:vertex 10 0 0)
    (gl:color 0 1 0) (gl:vertex 0 0 0) (gl:vertex 0 10 0)
    (gl:color 0 0 1) (gl:vertex 0 0 0) (gl:vertex 0 0 10)
    (gl:end)
    )
  )



(defun render ()
  ;;(gl:clear :color-buffer)
  (gl:clear :color-buffer-bit)
  (gl:clear-color 0 0.1 0.1 0.1)

  ;; render objects

  (render-axis)
  
  
  ;; look
  (gl:matrix-mode :modelview)
  (gl:load-identity)
  (glu:look-at 17 11 10 0 0 0 0 1 0)

  ;; flush
  (gl:flush)
  )

(defun set-viewport (width height)
  (gl:viewport 0 0 width height)

  ;; camera
  (gl:matrix-mode :projection)
  (gl:load-identity)
  ;;(gl:ortho -50 50 -50 50 -50 50)
  (glu:perspective 45.0 (/ width height) 1 40)

  ;; reset matrix-mode
  (gl:matrix-mode :modelview)
  (gl:load-identity))

(glfw:def-window-size-callback update-viewport (window w h)
  (declare (ignore window))
  (set-viewport w h)
  ;;(gli:init w h)
  )




(defun basic-window-example ()
  ;; Graphics calls on OS X must occur in the main thread
  (trivial-main-thread:with-body-in-main-thread ()
    (glfw:with-init-window (:title "Window test" :width 600 :height 400)
      
      ;; config for window
      (setf %gl:*gl-get-proc-address* #'glfw:get-proc-address)
      (glfw:set-key-callback 'quit-on-escape)
      (glfw:set-window-size-callback 'update-viewport)

      ;; init open gl
      (gl:clear-color 0 0 0 0)
      (set-viewport 600 400)

      (gl:matrix-mode :modelview)
      (gl:load-identity)
      
      
      ;; main loop
      (loop until (glfw:window-should-close-p)
            do (render)
            do (glfw:swap-buffers)
            do (glfw:poll-events))
      (quit-simulator)
      )))
