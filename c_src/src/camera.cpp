
#ifndef __CAMERA_CPP__
#define __CAMERA_CPP__

//#include <GL/glew.h>
#ifdef __APPLE_CC__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <cmath>


using namespace glm;

glm::vec4 crossVec4(glm::vec4 _v1, glm::vec4 _v2){
  vec3 vec1 = vec3(_v1[0], _v1[1], _v1[2]);
  vec3 vec2 = vec3(_v2[0], _v2[1], _v2[2]);
  vec3 res = cross(vec1, vec2);
  return glm::vec4(res[0], res[1], res[2], 0);
}


class DomainObject {
public:
  glm::vec4 displacement;
  glm::vec4 velocity;
  glm::vec4 acceleration;

  glm::vec4 rot_displacement;
  glm::vec4 rot_velocity;
  glm::vec4 rot_acceleration;

  float virtual_radius = 1.00;

  void update_massless_refrectless_complation_observer(){
    // update for domain object
    // which has no energy, no reflectable spins, no relativity to relative observers.
    velocity += acceleration;
    displacement += velocity;
    
    rot_velocity += rot_acceleration;
    rot_displacement += rot_velocity;
  }
    
};



class Camera : public DomainObject{ // default Camera, forcus looking point.
  glm::vec4 look_at = glm::vec4(0,0,0,0);
  
  double theta;      // determines the x and z positions
  double y;          // the current y position
  double dTheta;     // increment in theta for swinging the camera around
  double dy;         // increment in y for moving the camera up/down
public:
  /*
  Camera(): theta(0), y(3), dTheta(0.04), dy(0.2) {}

  double getX() {return 10 * cos(theta);}
  double getY() {return y;}
  double getZ() {return 10 * sin(theta);}
  void moveRight() {theta += dTheta;}
  void moveLeft() {theta -= dTheta;}
  void moveUp() {y += dy;}
  void moveDown() {if (y > dy) y -= dy;}
  */

  glm::vec4 updateCamera(){
    vec4 up = (crossVec4(normalize(look_at-displacement),
                         vec4(1, 0, 0, 0))
               );
    
    
    //glm::lookAt(displacement, look_at, up);
    
    return up;
  }
};





#endif // __CAMERA_CPP__
