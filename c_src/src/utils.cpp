

#ifndef __UTILS_CPP__
#define __UTILS_CPP__

#include "include_libs.hpp"
#include "utils.hpp"


void lightup(){
  static GLfloat position[] = {-10.0f, 10.0f, 10.0f, 1.0f};
  static GLfloat ambient [] = { 1.0f, 1.0f, 1.0f, 1.0f};
  static GLfloat diffuse [] = { 1.0f, 1.0f, 1.0f, 1.0f};
  static GLfloat specular[] = { 0.0f, 0.0f, 0.0f, 0.0f};
  glLightfv(GL_LIGHT0, GL_POSITION, position);
  glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
  glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
  glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);

}

void render_sphere(){
  // setup material
  GLfloat ambient  [] = { 0.1f, 0.1f, 0.1f, 1.0f};
  GLfloat diffuse  [] = { 1.0f, 0.0f, 0.0f, 1.0f};
  GLfloat specular [] = { 1.0f, 1.0f, 1.0f, 1.0f};
  GLfloat shininess[] = { 0.0f};
  glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
  glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
    
  // render sphere
  GLUquadric *quad;
  quad = gluNewQuadric();
  glPushMatrix(); {
    glTranslatef(2,2,0);
    float r = sin(__framecount__*0.01)*0.4 + 0.8;
    gluSphere(quad, r, 20, 20);
  }; glPopMatrix();

}

int render_ground_boxes (){
  glColor3f(1.0, 1.0, 1.0);
  glBegin(GL_LINES);
  for (GLfloat i = -2.5; i <= 2.5; i += 0.25) {
    glVertex3f(i, 0, 2.5); glVertex3f(i, 0, -2.5);
    glVertex3f(2.5, 0, i); glVertex3f(-2.5, 0, i+sin(__framecount__*0.01));
  }
  glEnd();

  return 0;
}

int render_xyz_axis (){
  glBegin(GL_LINES);
  {
    glColor3f(1, 0, 0); glVertex3f(0, 0, 0); glVertex3f(10, 0, 0);
    glColor3f(0, 1, 0); glVertex3f(0, 0, 0); glVertex3f(0, 10, 0);
    glColor3f(0, 0, 1); glVertex3f(0, 0, 0); glVertex3f(0, 0, 10);
  }
  glEnd();

  return 0;
}

#endif // __UTILS_CPP__
