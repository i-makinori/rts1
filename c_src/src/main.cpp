
#ifndef __MAIN_C__
#define __MAIN_C__

#include "include_libs.hpp"
#include "utils.cpp"

#include "vbo.cpp"
//#include "vbo.hpp"



static const std::string WINDOW_CAPTION = "SDL with OpenGL";
static const Uint32 WINDOW_WIDTH = 1200;
static const Uint32 WINDOW_HEIGHT = 800;
static const Uint32 WINDOW_BPP = 32;

static const Uint32 FPS = 60;

static SDL_Window* window;
static SDL_GLContext context;

bool init();
bool finalize();

void update();
void draw();
bool pollingEvent();
bool simulator_loop();


int main() {
  // initialize
  if (!init()) {
    std::cerr << "ERROR: failed to initialize SDL" << std::endl;
    exit(1);
  }

  // main loop
  simulator_loop();

  // finalize
  finalize();
  
  return 0;
}


bool init_SDL(){
  // initialize SDL
  if( SDL_Init(SDL_INIT_VIDEO) < 0 ) return false;

  // enable double buffering
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

  // create indow
  // set SDL_WINDOW_OPENGL to use opengl for drawing
  window = SDL_CreateWindow(WINDOW_CAPTION.c_str(),
                            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                            WINDOW_WIDTH, WINDOW_HEIGHT,
                            SDL_WINDOW_OPENGL);
  if (!window) return false;

  // create OpenGL context
  context = SDL_GL_CreateContext(window);
  if (!context) return false;

  return true;
}


bool init_openGL() {
  // setup viewport
  glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glEnable(GL_DEPTH_TEST);
  
  // init openGL
  init_glew_and_check();

  // init VBO
  initVBO();

  return true;
}


bool init() {
  
  bool init_sdl_p;

  // SDL
  init_sdl_p = init_SDL();  
  if (!init_sdl_p){
    std::cout << "failed to init SDL\n";
    return false;
  }

  // openGL
  init_openGL();

  return true;
}

bool finalize() {
  // finalize SDL
  SDL_Quit();

  // finalize openGL
  deleteVBO();

  return true;
}


void update() {
  __framecount__ += 1;
}



void draw() {
  // clear
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  
  // setup projection matrix
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0, (GLdouble) WINDOW_WIDTH / (GLdouble) WINDOW_HEIGHT, 2.0, 200.0);

  // and look
  gluLookAt(6, 8, 10, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

  // render world
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();


  // render light-object
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  
  lightup(); {
    render_sphere();
  };
  glDisable(GL_LIGHT0);
  glDisable(GL_LIGHTING);
  
  
  // render nolight-objects
  renderVBO();
  render_ground_boxes();
  render_xyz_axis();
}

// event handler
class EventHander{
 public:
  float mouse_x, mouse_y;
  bool mouse_right_pressed, mouse_right_pressing, mouse_right_released;
  bool mouse_left_pressed, mouse_left_pressing, mouse_left_released;
  bool mouse_middle_pressed, mouse_middle_pressing, mouse_middle_released;
  bool mouse_wheel_updown; // 0: natural, -1: up, +1: down
  
  char* key_pressed;
  char* key_pressing;
  char* key_released;

  void update_handler();
};


// polling event and execute actions
bool pollingEvent() {
  SDL_Event ev;
  SDL_Keycode key;
  while ( SDL_PollEvent(&ev) ) {
    // SDL_QUIT
    if(ev.type == SDL_QUIT){
      return false;
    }
    // KEYDOWN
    if(ev.type==SDL_KEYDOWN) {
      key = ev.key.keysym.sym;
      // ESC
      if(key == SDLK_ESCAPE){
        return false;
      }
    }
  }
  return true;
}

bool simulator_loop(){

  static const Uint32 interval = 1000 / FPS;
  static Uint32 nextTime = SDL_GetTicks() + interval;
  bool skipDraw = false;
  while (true) {
    // check event
    if (!pollingEvent()) break;

    // update and draw
    update();
    if (!skipDraw) {
      draw();
      SDL_GL_SwapWindow(window);
    }

    int delayTime = (int)(nextTime - SDL_GetTicks());
    if (delayTime > 0) {
      SDL_Delay((Uint32)delayTime);
      skipDraw = false;
    } else {
      // skip next draw step because of no time
      skipDraw = true;
    }

    nextTime += interval;
  }

  return true;
}



#endif // __MAIN_C__
