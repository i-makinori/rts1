
#ifndef __INCLUDE_LIBS_HPP__
#define __INCLUDE_LIBS_HPP__


// basic libs
#include <iostream>
#include <cmath>
#include <assert.h>
#include <cstdio>
#include <cstdlib>


// glew 
#include <GL/glew.h>


// glm
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective


// sdl2
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>



// variables

int __framecount__ = 0;



//
#endif // __INCLUDE_LIBS_HPP__


