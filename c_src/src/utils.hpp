
#ifndef __UTILS_HPP__
#define __UTILS_HPP__

#include "include_libs.hpp"

void lightup();
void render_sphere();
  
int render_ground_boxes();
int render_xyz_axis();

#endif // __UTILS_HPP__
