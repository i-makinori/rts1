
#ifndef __SRC_VBO_CPP__
#define __SRC_VBO_CPP__

#include "include_libs.hpp"

// GLEW initialize 

int init_glew_and_check () {
  // Init GLEW
  if ( glewInit() != GLEW_OK )
    {
      std::cerr << "Failed to initialize GLEW." << std::endl;
      exit(-1);
      return -1;
    }

  // check version and supporting-function
  if ( !glewIsSupported("GL_VERSION_1_5") && !glewIsSupported( "GL_ARB_vertex_buffer_object" ) ){
    std::cerr << "ARB_vertex_buffer_object not supported!" << std::endl;
    exit(-2);
    return -2;
  }
  
  return 0;
}



// example VBO

GLint vrtVBO;
GLint idxVBO;


struct VertexXYZColor {
  glm::vec4 m_Pos;
  glm::vec4 m_Color;
};

VertexXYZColor g_Vertices[8] = {
  {glm::vec4(+1, +1, +1, 0), glm::vec4(+1, +1, +1, 0)}, // 0
  {glm::vec4(-1, +1, +1, 0), glm::vec4(+0, +1, +1, 0)}, //+1
  {glm::vec4(-1, -1, +1, 0), glm::vec4(+0, +0, +1, 0)}, // 2
  {glm::vec4(+1, -1, +1, 0), glm::vec4(+0, +0, +1, 0)}, // 3
  {glm::vec4(+1, -1, -1, 0), glm::vec4(+1, +0, +1, 0)}, // 4
  {glm::vec4(-1, -1, -1, 0), glm::vec4(+0, +0, +0, 0)}, // 5
  {glm::vec4(-1, +1, -1, 0), glm::vec4(+0, +1, +0, 0)}, // 6
  {glm::vec4(+1, +1, -1, 0), glm::vec4(+1, +1, +0, 0)}  // 7
};

GLuint g_Indeces[24] = {
  0, 1, 2, 3,                 // Front face
  7, 4, 5, 6,                 // Back face
  6, 5, 2, 1,                 // Left face
  7, 0, 3, 4,                 // Right face
  7, 6, 1, 0,                 // Top face
  3, 2, 5, 4,                 // Bottom face
};

GLuint g_uiVerticesVBO = 0;
GLuint g_uiIndicesVBO = 0;


#define MEMBER_OFFSET(s,m) ((char *)NULL + (offsetof(s,m)))
#define BUFFER_OFFSET(i) ((char *)NULL + (i))

int initVBO() {
  // create VBO and get IDs.
  glGenBuffersARB(1, &g_uiVerticesVBO);
  glGenBuffersARB(1, &g_uiIndicesVBO);

  return 0;
  
}

int renderVBO() {

  // copy vertex data to VBO
  glBindBufferARB(GL_ARRAY_BUFFER_ARB, g_uiVerticesVBO);
  glBufferDataARB(GL_ARRAY_BUFFER_ARB, sizeof(g_Vertices), g_Vertices, GL_STATIC_DRAW_ARB);
  glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);

  // copy index data to VBO
  glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, g_uiIndicesVBO);
  glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER_ARB, sizeof(g_Indeces), g_Indeces, GL_STATIC_DRAW_ARB);
  glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, 0);

  // We need to enable the client stats for the vertex attributes we want 
  // to render even if we are not using client-side vertex arrays.
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);

  
  // Bind the vertices's VBO
  glBindBufferARB( GL_ARRAY_BUFFER_ARB, g_uiVerticesVBO );
  glVertexPointer( 3, GL_FLOAT, sizeof(VertexXYZColor), MEMBER_OFFSET(VertexXYZColor,m_Pos));
  glColorPointer( 3, GL_FLOAT, sizeof(VertexXYZColor), MEMBER_OFFSET(VertexXYZColor,m_Color)) ;

  // Bind the indices's VBO
  glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, g_uiIndicesVBO );
  glDrawElements( GL_QUADS, 24, GL_UNSIGNED_INT,  BUFFER_OFFSET( 0 ) );

  // Unbind buffers so client-side vertex arrays still work.
  glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, 0 );  
  glBindBufferARB( GL_ARRAY_BUFFER_ARB, 0 );

  // Disable the client side arrays again.
  glDisableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);

  return 0;
}


void deleteVBO(){
  if ( g_uiIndicesVBO != 0 ) {
      glDeleteBuffersARB( 1, &g_uiIndicesVBO );
      g_uiIndicesVBO = 0;
    }
  if ( g_uiVerticesVBO != 0 ) {
      glDeleteBuffersARB( 1, &g_uiVerticesVBO );
      g_uiVerticesVBO = 0;
    }
}


#endif // __SRC_VBO_CPP__

