
#ifndef __VBO_HPP__
#define __VBO_HPP__


int init_glew_and_check ();

// example VBO

// #define MEMBER_OFFSET(s,m) ((char *)NULL + (offsetof(s,m)))
// #define BUFFER_OFFSET(i) ((char *)NULL + (i))

int initVBO();
int renderVBO();
void deleteVBO();


#endif // __VBO_HPP__
