

(defsystem "rts1"
  :version "0.0.1"
  :author "Makinori Ikegami"
  :license ""
  :depends-on (cffi sdl2 cl-opengl cl-glu cl-glfw3 trivial-main-thread)
  ;;:depends-on (cffi cl-cffi-gtk cl-opengl cl-glu bordeaux-threads)
  :components ((:module "src"
                :components
                ((:file "main")
                 (:file "devel"))
                ))
  :description ""
  :in-order-to ((test-op (test-op "rts1/tests"))))

(defsystem "rts1/tests"
  :author "i-makinori"
  :license ""
  :depends-on (rts1
               rove)
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for rts1"
  :perform (test-op (op c) (symbol-call :rove :run c)))
